public class test {
    Node head;
    Node tail;
    
    //push() merupakan sebuah method yang berfungsi untuk menambahkan node pada stack
    public void push(char data) {
        //digunakan untuk membuat sebuah node baru
        Node baru= new Node(data);
        //apabila head sama dengan null maka node baru akan dideklarasikan sebagai head dan tail
        if (head == null) {
            head = tail = baru;
        } else {
            //pointer next pada node baru akan menunjuk ke head
            baru.next = head;
            //mendeklarasikan node baru sebagai head
            head = baru;
        }
    }

    //method display digunakan untuk menampilkan seluruh data yang telah di input
    public void display(){
    //membuat variabel current dengan tipe data node dan diinisialisasikan sebagai head
    Node current = head;
    //apabila head dalam kondisi kosong maka akan menampilkan output "kosong"
        if(head==null){
            System.out.print("Kosong");
        } else {
            //melakukan perulangan while apa bila current tidak sama dengan null
            while(current!=null){
                //menampilkan data pada variabel current
                System.out.print(current.data +" ");
                //memindahkan current ke pointer current selanjutnya
                current=current.next;
            }
        }
    }

    //method pop berfungsi untuk mengeluarkan data pada stack
    public Node pop() {
        //membuat variabel bantu dengan tipe data node yang diinisialisasikan sebagai head
        Node bantu = head;
        if (bantu == null) {
            return null;
        } else {
            //mengubah head menjadi pointer next head
            head = head.next;
            //mengubah nilai next pada variabel bantu menjadi null
            bantu.next = null;
        }
        return bantu;
    }

    //method post digunakan untuk mengubah infix menjadi postfix
    public void post(test Stack2, test Stack3){
         //membuat variabel bantu dengan tipe data node yang diinisialisasikan sebagai head
        Node bantu = head;
        //membuah sebuah objek baru bernama temp
        Node temp = new Node (' ');

        //melakukan perulangan jika variabel bantu tidak sama dengan null
        while(bantu!=null){
            
            if(bantu.data=='('||bantu.data=='-'||bantu.data=='+'||bantu.data=='*'){
                //apabila data pada variabel bantu sama dengan operator maka masukkan ke stack2
                Stack2.push(bantu.data);
            } else if (bantu.data=='A'||bantu.data=='B'||bantu.data=='C'||bantu.data=='D'||bantu.data=='E'||bantu.data=='F'||bantu.data=='G'){
                //apabila data pada variabel bantu sama dengan operand maka masukkan ke stack3
                Stack3.push(bantu.data);
            } else if (bantu.data==')'){
                //apabila data pada variabel bantu sama dengan ')' maka akan membuat objek baru
                temp = new Node(' ');
                //melakukan perulangan apabila data pada variabel temp tidak sama dengan ')'
                while(temp.data!='('){
                    //mengeluarkan variabel temp dari stack2
                    temp = Stack2.pop();
                    //memasukkan data pada variabel temp ke stack3
                    Stack3.push(temp.data);
                }
                //apabila data pada variabel temp sama dengan '(' maka akan mengeluarkan data pada variabel temp dari stack 3
                if(temp.data=='('){
                    temp = Stack3.pop();
                }}
                //memindahkan variabel bantu ke pointer next selanjutnya
            bantu=bantu.next;
        }
        //membuat variabel bantu2 dengan tipe data Node dan diinisialisasikan dengan head dari stack 2
        Node bantu2 = Stack2.head;
        //melakukan perulangan apabila variabel bantu2 tidak sama dengan null
        while(bantu2!=null){
            //memindhkan variabel temp dari stack2
            temp=Stack2.pop();
            //memasukkan data pada variabel temp ke dalam stack3
            Stack3.push(temp.data);
            //menginisialisasikan variabel bantu2 sebagai head dari stack2
            bantu2=Stack2.head;
      }
    }

    //method pre digunakan untuk mengubah infix menjadi prefix
    public void pre(test Stack2, test Stackpre){
        //membuat variabel bantu dengan tipe data node yang diinisialisasikan sebagai head
        Node bantu = head;
        //membuah sebuah objek baru bernama temp
        Node temp = new Node (' ');
        //melakukan perulangan jika variabel bantu tidak sama dengan null 
        while(bantu!=null){
            if(bantu.data=='-'||bantu.data=='+'||bantu.data=='*'){
                //apa bila data pada variabel bantu sama dengan operator maka data pada variabel bantu akan di push ke stackpre
                Stackpre.push(bantu.data);
                //apabila head pada stack 2 tidak sama dengan null maka 
                if(Stack2.head!=null){
                    //apabila head pada stack2 tidak sama dengan null maka data pada variabel temp akan dekeluarkan dari stack2
                    temp = Stack2.pop();
                    //setelah dikeluarkan maka akan dimasukan di stackpre
                    Stackpre.push(temp.data);
                }
                if(Stack2.head!=null){
                    //apabila head pada stack2 tidak sama dengan null maka data pada variabel temp akan dekeluarkan dari stack2
                    temp = Stack2.pop();
                    //setelah dikeluarkan data pada variabel temp akan dimasukkan di stackpre
                    Stackpre.push(temp.data);
                }
            } else if (bantu.data=='A'||bantu.data=='B'||bantu.data=='C'||bantu.data=='D'||bantu.data=='E'||bantu.data=='F'||bantu.data=='G'){
                //apabila data pada variabel bantu merupakan operand, maka akan di tambahkan ke stack2
                Stack2.push(bantu.data);
            }
           //memindahkan variabel bantu ke pointer next selanjutnya 
            bantu=bantu.next;
        }
        //membuat variabel bantu2 dengan tipe variabel Node yang diinisialisasikan dengan head pada stack2 
        Node bantu2 = Stack2.head;
        //melakukan perulangan apabila bantu2 tidak sama dengan null
        while(bantu2!=null){
            //mengeluarkan data pada variabel temp dari stack2
            temp = Stack2.pop();
            //memsukkan data pada variabel temp ke dalam stackpre 
            Stackpre.push(temp.data);
            //menginisialisasikan variabel bantu2 dengan head dari stack2
            bantu2=Stack2.head;
        }
    }

    public static void main(String[] args) {
        test Stack1 = new test();
        test Stack2 = new test();
        test Stack3 = new test();
        test Stack4 = new test();
        test Stackpost = new test();
        test Stackpre = new test();

        String Soal;

        Soal = "(A+B)*C-(D-E)*(F+G)";

        //memasukkan infix ke dalam array
        char[] perc = Soal.toCharArray();
        for (int i = Soal.length() - 1; i >= 0; i--) {
            Stack1.push(perc[i]);
        }


        Stack1.post(Stack2, Stack3);
        System.out.println("====Convert Infix to Postfix and Prefix====");
        Stack1.display();
        System.out.println(" ");

        //menginisialisasikan variabel stack dengan variabel Node dengan head dari stack3
        Node stack = Stack3.head;
        
        //melakukan perulangan apabila stack tidak sama dengan null
        while (stack != null) {
            //memasukkan data pada stack kedalam stackpost
            Stackpost.push(stack.data);
            //memindahkan variabel stack ke pointer next selanjutnya 
            stack = stack.next;
        }

        //melakukan print postfix
        System.out.println(" ");
        System.out.println("Posfix");
        Stackpost.display();
        System.out.println(" ");

        Stack3.pre(Stack2, Stack4);
        //menginisialisasikan variabel stack1 bertipe data node sebagai head dari stack4
        Node stack1 = Stack4.head;

        //melakukan perulangan apabila stack1 tidak sama dengan null
        while (stack1 != null) {
            //memasukkan data pada stack1 kedalam stackpre
            Stackpre.push(stack1.data);
            //memindahkan variabel stack1 ke pointer next selanjutnya 
            stack1 = stack1.next;
        }

        //melakukan print prefix
        System.out.println(" ");
        System.out.println("Prefix");
        Stackpre.display();
    }
}